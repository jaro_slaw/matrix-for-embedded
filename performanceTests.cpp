/*
 * performanceTests.cpp
 *
 *  Created on: Sep 16, 2016
 *      Author: mkielanx
 */

#include <iostream>
#include <gtest/gtest.h>
#include <random>
#include <algorithm>
#include <functional>
#include <memory>
#include <chrono>
#include <tuple>
#include "matrix.hpp"

using MatrixDataType = int;

static constexpr std::size_t i[]
{50, 100, 200, 300, 400, 500, 600, 700, 800};

static Matrix<MatrixDataType, i[0], i[0]> A0, B0;
static Matrix<MatrixDataType, i[1], i[1]> A1, B1;
static Matrix<MatrixDataType, i[2], i[2]> A2, B2;
static Matrix<MatrixDataType, i[3], i[3]> A3, B3;
static Matrix<MatrixDataType, i[4], i[4]> A4, B4;
static Matrix<MatrixDataType, i[5], i[5]> A5, B5;
static Matrix<MatrixDataType, i[6], i[6]> A6, B6;
static Matrix<MatrixDataType, i[7], i[7]> A7, B7;
static Matrix<MatrixDataType, i[8], i[8]> A8, B8;

class MatrixPerformanceTest : public ::testing::Test
{
  protected:

    void SetUp() override
    {
      fillRand(A0);  fillRand(B0);
      fillRand(A1);  fillRand(B1);
      fillRand(A2);  fillRand(B2);
      fillRand(A3);  fillRand(B3);
      fillRand(A4);  fillRand(B4);
      fillRand(A5);  fillRand(B5);
      fillRand(A6);  fillRand(B6);
      fillRand(A7);  fillRand(B7);
      fillRand(A8);  fillRand(B8);
    }

    void TearDown() override {}

    template<typename T, std::size_t I, std::size_t J>
    void print(const Matrix<T, I, J>& A)
    {
      for (std::size_t i = 0; i < I; i++)
      {
        for (std::size_t j = 0; j < J; j++)
        {
          std::cout << A[i][j] << " ";
        }
        std::cout << std::endl;
      }
    }

    template<typename T, std::size_t i, std::size_t j>
    void fillRand(Matrix<T, i, j>& A)
    {
      std::default_random_engine dre{std::random_device{}()};
      std::uniform_int_distribution<T> ud;

      for(auto& a : A)
      {
        a.fill(ud(dre));
      }
    }


};


TEST_F(MatrixPerformanceTest, multiply_time)
{
  std::cout << "size" << '\t' << "time[ms]" << std::endl;

  auto f = [](auto& A, auto& B, int i)
  {
    auto begin = std::chrono::steady_clock::now();
    auto C = A * B;
    auto end = std::chrono::steady_clock::now();
    (void)C;
    std::cout
    << i
    << '\t'
    << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
    << std::endl;
  };

  f(A0, B0, i[0]);
  f(A1, B1, i[1]);
  f(A2, B2, i[2]);
  f(A3, B3, i[3]);
  f(A4, B4, i[4]);
  f(A5, B5, i[5]);
  f(A6, B6, i[6]);
  f(A7, B7, i[7]);
  f(A8, B8, i[8]);
}


TEST_F(MatrixPerformanceTest, LU_time)
{
  std::cout << "size" << '\t' << "time[ms]" << std::endl;

  auto f = [](auto& A, int i)
  {
    auto begin = std::chrono::steady_clock::now();
    auto C = lu(A);
    auto end = std::chrono::steady_clock::now();
    (void)C;

    std::cout
    << i
    << '\t'
    << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
    << std::endl;
  };

  f(A0, i[0]);
  f(A1, i[1]);
  f(A2, i[2]);
  f(A3, i[3]);
  f(A4, i[4]);
  f(A5, i[5]);
  f(A6, i[6]);
  f(A7, i[7]);
  f(A8, i[8]);
}


int main(int argc, char* argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
