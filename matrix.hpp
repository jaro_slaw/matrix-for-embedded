/*
 * matrix.hpp
 *
 *  Created on: Sep 12, 2016
 *      Author: Michal Kielan
 * Description: Simple matrix library
 */

#ifndef MATRIX_HPP_
#define MATRIX_HPP_

#include <array>
#include <utility>
#include <cmath>


/**
 * \ T type template, data must be a number only
 */
template<typename T>
using Arithmetic = typename std::enable_if<std::is_arithmetic<T>::value, T>::type;


/**
 * \ Matrix type definition
 * \ use like this: Matrix<Type, ROW, COL>
 */
template <typename T, std::size_t i, std::size_t j>
using Matrix = std::array<std::array<T, j>, i>;


/**
 * \ Create a matrix filling with data
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> fill(T data)
{
  Matrix<T, i, j> A{};
//  std::fill(A[0][0], A[i][j], data);
  for(auto& a : A)
  {
    a.fill(data);
  }

  return A;
}


/**
 * \ Create a matrix filling with one
 */
template<unsigned int, std::size_t i>
Matrix<unsigned int, i, i> ones()
{
  return fill<unsigned int, i, i>(1);
}


/**
 * \ Create a matrix filling with zero
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> zeros()
{
  return fill<T, i, j>(static_cast<T>(0));
}


/**
 * \ Create a matrix with number at the diagonal, the default value is one
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> eye(T data = 1)
{
  auto eye = fill<T, i, i>(0);

  for (std::size_t it = 0; it < i; it++)
  {
    eye[it][it] = data;
  }
  return eye;
}


/**
 * \ Sum of two matrices
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> sum(const Matrix<T, i, j>& A, const Matrix<T, i, j>& B)
{
  Matrix<T, i, j> sum{};

  for(std::size_t it = 0; it < i; it++)
  {
    for(std::size_t jt = 0; jt < j; jt++)
    {
      sum[it][jt] = A[it][jt] + B[it][jt];
    }
  }
  return sum;
}


/**
 * \ Sum matrix and scalar
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> sum(const Matrix<T, i, j>& A, T& scalar)
{
  Matrix<T, i, j> sum{};

  for(std::size_t it = 0; it < i; it++)
  {
    for(std::size_t jt = 0; jt < j; jt++)
    {
      sum[it][jt] = A[it][jt] + scalar;
    }
  }
  return sum;
}


/**
 * \ Substraction of two matrices
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> sub(const Matrix<T, i, j>& A, const Matrix<T, i, j>& B)
{
  Matrix<T, i, j> sub{};

  for(std::size_t it = 0; it < i; it++)
  {
    for(std::size_t jt = 0; jt < j; jt++)
    {
      sub[it][jt] = A[it][jt] - B[it][jt];
    }
  }
  return sub;
}


/**
 * \ Substraction matrix and scalar
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> sub(const Matrix<T, i, j>& A, T& scalar)
{
  Matrix<T, i, j> sub{};

  for(std::size_t it = 0; it < i; it++)
  {
    for(std::size_t jt = 0; jt < j; jt++)
    {
      sub[it][jt] = A[it][jt] - scalar;
    }
  }
  return sub;
}


/**
 * \ Multiply of two matrices
 */
template<typename T,  std::size_t n, std::size_t m, std::size_t p>
Matrix<T, n, p> mult(const Matrix<T, n, m>& A, const Matrix<T, m, p>& B)
{
  auto prod = zeros<T, n, p>();

  for(std::size_t i = 0; i < n; ++i)
 {
    for(std::size_t j = 0; j < p; ++j)
    {
      for(std::size_t k = 0; k < m; ++k)
      {
        prod[i][j] += (A[i][k] * B[k][j]);
      }
    }
  }
  return prod;
}


/**
 * \ Multiplication matrix and scalar
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> mult(const Matrix<T, i, j>& A, T& scalar)
{
  Matrix<T, i, j> prod{};

  for(std::size_t it = 0; it < i; it++)
  {
    for(std::size_t jt = 0; jt < j; jt++)
    {
      prod[it][jt] = A[it][jt] * scalar;
    }
  }
  return prod;
}


/**
 * \ Multiplication matrix and scalar
 */
template<typename T, std::size_t n, std::size_t m>
Matrix<T, n, m> div(const Matrix<T, n, m>& A, T& scalar)
{
  if(scalar == 0)
    return;

  Matrix<T, n, m> prod{};

  for(std::size_t i = 0; i < n; i++)
  {
    for(std::size_t j = 0; j < m; j++)
    {
      prod[i][j] = A[i][j] / scalar;
    }
  }
  return prod;
}


/*
 * \ LU decomposition of matrix A, return pair first - L, second - U
 */
template<typename T, std::size_t n>
std::pair<Matrix<T,n,n>, Matrix<T,n,n>>lu(const Matrix<T,n,n>& A)
{
  auto L = eye<T, n, n>();
  auto U = zeros<T, n, n>();
  T sum;

  for(std::size_t j = 0; j < n; j++)
  {
    for(std::size_t i = 0; i <= j; i++)
    {
      sum = 0.0;
      for(std::size_t p = 0; p < i; p++)
      {
        sum += U[p][j] * L[i][p];
      }
      U[i][j] = A[i][j] - sum;
    }
    for(std::size_t i = j; i < n; i++)
    {
      sum = 0.0;
      for(std::size_t p = 0; p < i; p++)
      {
        sum += U[p][j] * L[i][p];
      }

      if(U[j][j] == 0)
        throw;
      else
        L[i][j] = (A[i][j] - sum) / U[j][j];
    }
  }

  return std::pair<Matrix<T,n,n>, Matrix<T,n,n>>{L, U};
}


/**
 * \ Determinant of matrix
 */
template<typename T, std::size_t n>
T det(const Matrix<T, n, n>& A)
{
  if (n == 0)
  {
    return 0;
  }

  else if (n == 1)
  {
    return 1;
  }

  else if (n == 2)
  {
    auto&& a = A[0][0]; auto&& b = A[0][1];
    auto&& c = A[1][0]; auto&& d = A[1][1];

    return a*d - b*c;
  }

  else if (n == 3)
  {
    auto&& a = A[0][0]; auto&& b = A[0][1]; auto&& c = A[0][2];
    auto&& d = A[1][0]; auto&& e = A[1][1]; auto&& f = A[1][2];
    auto&& g = A[2][0]; auto&& h = A[2][1]; auto&& i = A[2][2];

    return a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h;
  }

  else
  {
    // TODO do it using LU algorithm (faster)
   // auto LU = lu(A);
  //  auto&& L = LU.first;
   // auto&& U = LU.second;


    return -1;
  }
}


/**
 * \ Inverse matrix
 */
template<typename T, std::size_t n>
Matrix<T, n, n> inv(const Matrix<T, n, n>& A)
{
  Matrix<T, n, n> invA{};

  // TODO

  return invA;
}


/**
 * \ Check if the matrix is identity
 */
template<typename T, std::size_t i>
bool isIdentity(const Matrix<T, i, i>& A)
{
  auto identity = eye<unsigned int, i, i>();
  auto uiA = static_cast<Matrix<unsigned int, i, i>>(A);
  return (uiA == identity);
}


/**
 * \ Transpose of matrix
 */
template<typename T, std::size_t n, std::size_t m>
Matrix<T, m, n> trans(Matrix<T, n, m> A)
{
	Matrix<T, m, n> transA{};

	for (std::size_t i = 0; i < n; i++)
	{
		for (std::size_t j = 0; j < m; j++)
		{
			transA[j][i] = A[i][j];
		}
	}

	return transA;
}


/**
 * \ Create a matrix with number at the diagonal, the default value is one
 */
template<typename T, typename P, std::size_t i, std::size_t j>
Matrix<T, i, j> pow(const Matrix<T, i, j>& A, P power)
{
  Matrix<T, i, j> powA;

  for (std::size_t it = 0; it < i; it++)
  {
    for (std::size_t jt = 0; jt < j; jt++)
    {
      powA[it][jt] = std::pow(A[it][jt], power);
    }
  }

  return powA;
}


/**
 * \ Absolut value
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> abs(const Matrix<T, i, j>& A)
{
  Matrix<T, i, j> absA;

  for (std::size_t it = 0; it < i; it++)
  {
    for (std::size_t jt = 0; jt < j; jt++)
    {
      absA[it][jt] = std::abs(A[it][jt]);
    }
  }

  return absA;
}


/**
 * \ Overloaded '+' operator of sum two matrices
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> operator+(const Matrix<T, i, j>& A, const Matrix<T, i, j>& B)
{
	return sum(A, B);
}


/**
 * \ Overloaded '+' operator for add matrix and scalar
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> operator+(const Matrix<T, i, j>& A, T scalar)
{
  return sum(A, scalar);
}


/**
 * \ Overloaded '+' operator for add scalar and matrix
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> operator+(T scalar, const Matrix<T, i, j>& A)
{
  return sum(A, scalar);
}


/**
 * \ Overloaded '-' operator for substraction two matrices
 */
template<typename T, std::size_t i, std::size_t j>
Matrix<T, i, j> operator-(const Matrix<T, i, j>& A, const Matrix<T, i, j>& B)
{
	return sub(A, B);
}


/**
 * \ Overloaded '-' operator for substraction matrix and scalar
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> operator-(const Matrix<T, i, j>& A, T scalar)
{
  return sub(A, scalar);
}


/**
 * \ Overloaded '*' operator for multiply two matrices
 */
template<typename T,  std::size_t n, std::size_t m, std::size_t p>
Matrix<T, n, p> operator*(const Matrix<T, n, m>& A, const Matrix<T, m, p>& B)
{
	return mult(A, B);
}


/**
 * \ Overloaded '*' operator for multiply matrix and scalar
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> operator*(const Matrix<T, i, j>& A, T scalar)
{
  return mult(A, scalar);
}


/**
 * \ Overloaded '*' operator for multiply scalar and matrix
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> operator*(T scalar, const Matrix<T, i, j>& A)
{
  return mult(A, scalar);
}


/**
 * \ Overloaded '/' operator for divide matrix and scalar
 */
template<typename T,  std::size_t i, std::size_t j>
Matrix<T, i, j> operator/(const Matrix<T, i, j>& A, T scalar)
{
  return div(A, scalar);
}


/**
 * \ Compare two matrices with the same size, specific error
 */
template<typename T, std::size_t i, std::size_t j>
bool compare(const Matrix<T, i, j>& A, const Matrix<T, i, j>& B, T epsilon)
{
  auto Err = fill<T, i, j>(epsilon);
  return abs(A - B) < Err;
}

#endif /* MATRIX_HPP_ */
