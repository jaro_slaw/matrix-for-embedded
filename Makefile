CXX=g++

CXXFLAGS=

OPTIMIZATION=

CPPSTANDARD=c++14

CPPFLAGS=-Wall -g -flto


CXXFLAGS+=-std=$(CPPSTANDARD) 
CXXFLAGS+=$(CPPFLAGS)
CXXFLAGS+=$(OPTIMIZATION)

LFLAGS=-lgtest

all:
	 $(CXX) $(CXXFLAGS) correctTests.cpp -o correctTests $(LFLAGS)
	 $(CXX) ${CXXFLAGS} performanceTests.cpp -o performanceTests $(LFLAGS)

